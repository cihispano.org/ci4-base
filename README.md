# Aplicación base de CodeIgniter 4

## Instalación

Descargar el proyecto desde el repositorio.

Inicinicializa el repositorio git

```bash
user@localhost:$ git init
```

Inicializa composer:

```bash
user@localhost:$ composer install
```

## Configuración

Durante el proceso anterior, se ha creado el fichero de configuración `.env`. Dedes configurar los siguientes parámetros:

- CI_ENVIRONMENT
- app.baseURL

Si estás configurando el entorno de desarrollo, `CI_ENVIRONMENT` debe tener el valor `development`.

Para poder arrancar el proyecto con el servidor integrado de Codeigniter 4, en `app.baseURL` debes poner el valor `http://localhost:8080`

## Requisitos del servidor

PHP 7.2 o superior.

Para comprobar si tu servidor cumple con todas las dependendencias en caso de que hayas añadido algún paquete, puedes ejecutar el comando:

```bash
user@localhost:$ composer check-platform-reqs
```

Si quieres comprobar las dependiencias que no son de desarrollo, puedes añadir al final del comando anterior el modificador `--no-dev`

### Depedencias en el entorno de desarrollo

- ext-curl
- ext-dom
- ext-filter
- ext-intl
- ext-json
- ext-libxml
- ext-mbstring
- ext-Phar
- ext-SimpleXML
- ext-tokenizer
- ext-xml
- ext-xmlwriter

### Dependiencas en el entorno de producción

- ext-curl
- ext-intl
- ext-json
- ext-mbstring
